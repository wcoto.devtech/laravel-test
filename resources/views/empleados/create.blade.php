@extends('layouts.app')
@section('title', 'Menú 2')
@section('content')

<div class="container">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Agregar un nuevo empleado</h2>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('empleados.store') }}" method="POST">
    @csrf
        <div class="container">

            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Nombre</label>
                <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Ingrese su nombre" value="{{old('nombre')}}">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Apellido</label>
                <input type="text" class="form-control" name="apellido" id="apellido" placeholder="Ingrese su Apellido" value="{{old('apellido')}}">
            </div>
        
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Puesto de trabajo</label>
                    <select class="form-control selectorWapis" id="puesto" name="puesto_id">
                        <option value="" selected disabled hidden>Seleccione el puesto *</option>
                        @foreach ($puestos as $puesto)
                            @if (old('puesto')==$puesto->id)                                     
                                <option  value="{{$puesto->id}}" selected>{{ $puesto->name }}</option>
                            @else
                                <option  value="{{$puesto->id}}">{{ $puesto->name }}</option>
                            @endif                                
                        @endforeach
                    </select>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Telefono</label>
                <input type="Number" maxlength="8" class="form-control"name="numero_fijo" id="numero_fijo" placeholder="22222222" value="{{old('numero_fijo')}}">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Telefono Celular</label>
                <input maxlength="2" type="Number" class="form-control" name="numero_celular" id="numero_celular" placeholder="77777777" value="{{old('numero_celular')}}">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Direccion</label>
                <input type="text" class="form-control" name="direccion" id="direccion" placeholder="Calle San Antonio Colonia San Jacinto" value="{{old('direccion')}}">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">DUI</label>
                <input type="text" class="form-control" maxlength="10" name="dui" id="dui" placeholder="012345678-9" value="{{old('dui')}}">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Email address</label>
                <input type="email" class="form-control" name="correo_electronico" id="correo_electronico" placeholder="ejemplo@example.com" value="{{old('correo_electronico')}}">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Fecha de Nacimiento</label>
                <input type="Date" class="form-control" name="fecha_de_nacimiento" id="fecha_de_nacimiento" placeholder="DD/MM/AAAA" value="{{old('fecha_de_nacimiento')}}">
            </div>
            <button type="submit" class="btn btn-primary">Guardar</button>          
            <a class="btn btn-danger" href="{{route('empleados.index')}}">Cancelar</a>  
        </div>

    
   
</form>


@endsection