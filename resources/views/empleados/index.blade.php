@extends('layouts.app')
@section('title', 'Menú 2')
@section('content')

<div class="container">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Listado de Empleados</h2>
        </div>
    </div>
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('empleados.create') }}"> Agregar un Nuevo Empleado</a>
        </div>
    </div>
    
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="container">
        <table class="table table-hover">
        <thead>
            <tr>
            <th scope="col">Codigo</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellido</th>
            <th scope="col">Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($empleados as $empleado)
            <tr>
                <td>{{$empleado->id}}</td>
                <td>{{$empleado->nombre}}</td>
                <td>{{$empleado->apellido}}</td>
                <td>
                    <form action="{{route('empleados.destroy', $empleado)}}" method="POST">
                        @method('DELETE')
                        @csrf
                        <input type="submit" value="Eliminar" class="btn btn-sm btn-danger" onclick="return confirm('¿Quiere eliminar este cliente?')">
                    </form>
                </td>
            </tr>
          
            @endforeach

        </tbody>
        </table>
</div>   



@endsection