<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class PuestoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('puestos')->insert([
            'name' => 'Desarrollador back-end',
        ]);
        DB::table('puestos')->insert([
            'name' => 'Desarrollador front-end',
        ]);
        DB::table('puestos')->insert([
            'name' => 'Programador en PHP',
        ]);
        DB::table('puestos')->insert([
            'name' => 'Programador en Java',
        ]);
        DB::table('puestos')->insert([
            'name' => 'Programador en Python',
        ]);
        DB::table('puestos')->insert([
            'name' => 'Lider de Proyecto',
        ]);
        DB::table('puestos')->insert([
            'name' => 'Diseñador UX',
        ]);
    }
}
