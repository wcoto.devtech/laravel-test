<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateEmpleadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'=>'required',
            'apellido'=>'required',
            'puesto_id'=>'required',
            'numero_fijo'=>'required',
            'numero_celular'=>'required',
            'direccion'=>'required',
            'dui'=>'required',
            'correo_electronico'=>'required',
            'fecha_de_nacimiento'=>'required',
        ];
    }
    public function messages()
 
    {
        return [
            'nombre.required'=>'Debe ingresar el nombre del Empleado',
            'apellido.required'=>'Debe ingresar el apellido del Empleado',
            'puesto_id.required'=>'Debe seleccionar el puesto del Empleado',
            'numero_fijo.required'=>'Debe ingresar el numero fijo',
            'numero_celular.required'=>'Debe ingresar el numero de celular',
            'direccion.required'=>'Debe ingresar la direccion',
            'dui.required'=>'Debe ingresar el DUI',
            'correo_electronico.required'=>'Debe ingresar el correo electronico',
            'fecha_de_nacimiento.required'=>'Debe ingresar la fecha de Nacimiento',
        ];
    }
}
