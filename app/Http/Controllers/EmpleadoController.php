<?php

namespace App\Http\Controllers;

use App\Empleado;
use App\Http\Requests\CreateEmpleadoRequest;
use App\Puesto;
use Illuminate\Http\Request;

class EmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empleados = Empleado::latest()->paginate(5);
  
        return view('empleados.index',['empleados'=>$empleados]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $puestos= \App\Puesto::all();

        return view ('empleados.create', ['puestos'=>$puestos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEmpleadoRequest $request)
    {
        /*Empleado::create([
            'nombre'=>$request->nombre,
            'apellido'=>$request->apellido,
            'puesto_id'=>$request->puesto_id,
            'numero_fijo'=>$request->numero_fijo,
            'numero_celular'=>$request->numero_celular,
            'direccion'=>$request->direccion,
            'dui'=>$request->dui,
            'correo_electronico'=>$request->correo_electronico,
            'fecha_de_nacimiento'=>$request->fecha_de_nacimiento,
        ]);*/
        Empleado::create($request->validate());
   
        return redirect()->route('empleados.index')
                        ->with('success','Empleado asignado successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function show(Empleado $empleado)
    {
        return view('empleados.show',compact('empleado'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function edit(Empleado $empleado)
    {
        return view('empleados.edit',compact('empleado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Empleado $empleado)
    {
        $request->validate([
            'nombre'=>'required',
            'apellido'=>'required',
            'puesto_id'=>'required',
            'numero_fijo'=>'required',
            'numero_celular'=>'required',
            'direccion'=>'required',
            'dui'=>'required',
            'correo_electronico'=>'required',
            'fecha_de_nacimiento'=>'required',
        ]);

        $empleado->update($request->all());
  
        return redirect()->route('empleados.index')
                        ->with('success','Empleado updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function destroy(Empleado $empleado)
    {
        $empleado->delete();
  
        return redirect()->route('empleados.index')
                        ->with('success','Empleado deleted successfully');
    }
}
